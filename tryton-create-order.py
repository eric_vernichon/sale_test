#!/usr/bin/python
# -*- encoding: utf-8 -*-
import os,sys
sys.path.append('/opt/tryton/trytond')


import time
from random import randint
from decimal import Decimal
import trytond
from trytond.config import CONFIG
# Load the configuration file
CONFIG.update_etc('/opt/tryton/trytond/etc/trytond.conf')# Replace with your path
from trytond.pool import Pool
#from trytond.model import Cache
from trytond.cache import Cache
from trytond.transaction import Transaction
Pool.start()
dbname = "tryton_test"
CONTEXT = {}

pool = Pool(dbname)
Cache.clean(dbname)
range_line = [1001, 3001, 7001, 10001]
# Instantiate the pool
with Transaction().start(dbname, 0, context=CONTEXT):
    pool.init()

# User 0 is root user. We use it to get the admin id:
with Transaction().start(dbname, 0, context=CONTEXT):
    user_obj = pool.get('res.user')
    user = user_obj.search([('login', '=', 'admin')], limit=1)[0]
    user_id = user.id

with Transaction().start(dbname, user_id, context=CONTEXT) as transaction:

    #config = tryton_config.set_xmlrpc('http://admin:admin@127.0.0.1:8090/tryton_test')
    sale = pool.get('sale.sale')
    #print dir(sale)
    sale_line = pool.get('sale.line')
    commande = sale.search([('id', '=', 1)])
    ligne_commande = sale_line.search([('sale', '=', 1)])
    for y in range_line:
        start = time.time()
        new_commande = sale()
        new_commande.company = commande[0].company
        new_commande.currency = commande[0].currency
        new_commande.party = commande[0].party
        new_commande.sale_date = commande[0].sale_date
        new_commande.payment_term = commande[0].payment_term
        new_commande.invoice_address = commande[0].invoice_address
        new_commande.shipment_address = commande[0].shipment_address
        unit = ligne_commande[randint(0,1)].unit
        print y,",","Create order",",", time.time() - start
        for x in range(y):
            new_line = sale_line() #new_commande.lines.new()#quantity=Decimal(12.5), unit_price = Decimal(12.0))
            new_line.product = ligne_commande[randint(0,1)].product
            new_line.description = ligne_commande[randint(0,1)].description
            new_line.sale = new_commande
            new_line.quantity = randint(1,1000)
            new_line.unit = unit
            new_line.type = 'line'
            new_line.unit_price = Decimal(randint(1,100))
            new_line.save()
            if x % 1000 == 0:
                print y ,",",x ,",", time.time() - start
        new_commande.save()
        sale.quote([new_commande])
        print y ,",","A - Quotation" ,",",time.time() - start
        sale.confirm([new_commande])
        print y ,",","B - Confirm" ,",",time.time() - start
        sale.proceed([new_commande])
        print y ,",","C - Process" ,",",time.time() - start
        transaction.cursor.commit()
        print
        print

Cache.resets(dbname)

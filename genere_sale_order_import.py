#!/usr/bin/python
# -*- coding: utf-8 -*-
from xml.dom.minidom import parse
import sys
import time
from random import randint
import datetime
import sys
sys.path.append('/home/openerp/6.1/server')

import openerp
import logging
__author__ = openerp.release.author
__version__ = openerp.release.version
_logger = logging.getLogger('openerp')
def report_configuration():
    """ Log the server version and some configuration values.

    This function assumes the configuration has been initialized.
    """
    config = openerp.tools.config
    _logger.info("OpenERP version %s", __version__)
    for name, value in [('addons paths', config['addons_path']),
                        ('database hostname', config['db_host'] or 'localhost'),
                        ('database port', config['db_port'] or '5432'),
                        ('database user', config['db_user'])]:
        _logger.info("%s: %s", name, value)

dbname = 'test61'
 

openerp.tools.config.parse_config(['-d %s' % dbname,'--xmlrpc-port=9099', '--no-netrpc'])
openerp.netsvc.init_logger()
report_configuration()
config = openerp.tools.config

openerp.service.start_services()
for m in openerp.conf.server_wide_modules:
    try:
        openerp.modules.module.load_openerp_module(m)
    except Exception:
        msg = ''
        _logger.exception('Failed to load server-wide module `%s`.%s', m, msg)

cr = openerp.pooler.get_db(dbname).cursor()
pool = openerp.pooler.get_pool(cr.dbname)
number_of_iteration = 10001
number_of_order = 1
copy_sale_id = 1
user="admin"
pwd="admin"
uid = 3
sale_order_obj = pool.get('sale.order')
sale_order_line_obj = pool.get('sale.order.line')
res_partner_obj = pool.get('res.partner')
res_partner_address_obj = pool.get('res.partner.address')
product_product_obj = pool.get('product.product')
product_pricelist_obj = pool.get('product.pricelist')

 

sale_ids = sale_order_obj.search(cr,uid,[])
partner_ids = res_partner_obj.search(cr, uid,[('customer', '=', True)])
len_partner = len(partner_ids) - 1
partner = partner_ids[randint(0, len_partner)]

product_pricelist_ids  = product_pricelist_obj.search(cr,uid, [('type', '=', 'sale')])
len_product_pricelist = len(product_pricelist_ids) - 1
pricelist = product_pricelist_ids[randint(0, len_product_pricelist)]

partner_address_ids = res_partner_address_obj.search(cr, uid, [('partner_id', '=', partner)])
len_partner_address = len(partner_address_ids) - 1
partner_address = partner_address_ids[randint(0, len_partner_address)]

product_product_ids  = product_product_obj.search(cr, uid , [('sale_ok', '=', True)])
len_product_product = len(product_product_ids) - 1
product_datas = {}
for id in product_product_ids:
    product_datas[id] = product_product_obj.read(cr,uid, id,  ['name','list_price'])



for y in range(number_of_order):
    start = time.time()
    
    sale_id = sale_order_obj.create(cr, uid,  {'partner_shipping_id': partner_address,'partner_invoice_id': partner_address,'partner_order_id': partner_address,'pricelist_id': pricelist,'partner_id':partner,'date_order': datetime.datetime.now().strftime('%d/%m/%Y')})
  
    print "Sale id : ", sale_id
    print "Create Order,",   time.time() - start
    for x in range(number_of_iteration):
        product = product_product_ids[randint(0, len_product_product)]
        product_data = product_datas[product]
        sale_line_ids = sale_order_line_obj.create(cr, uid, {'price_unit':product_data['list_price'],'product_uom_qty':randint(1,1000), 'order_id' :  sale_id,'product_id':product,'name':product_data['name']})
        if x%1000 == 0 and x>0:
            print  x , ",",   time.time() - start

    sale_order_obj.action_wait(cr, uid, [sale_id])
    print "confirm" , ",", time.time() - start
    cr.commit()
    sale_order_obj.action_invoice_create(cr, uid, [sale_id])
    print "invoice",",",time.time() - start
    cr.commit()
    print "fin"
    
